package main.controllers;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import main.model.DESHash;
import main.model.FileSource;
import main.utils.HexTextFilter;
import main.utils.MyUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public static final int START = 1, ADDITION = 2, DES = 3, END = 4;

    public StackPane root;

    public TextField keyField;
    public TextField encryptedField;
    public TextField bufferField;
    public TextField initializationField;
    public TextField textField;

    public Label filePath;
    public Label length;

    public Button load;
    public Button generateKey;
    public Button makeStep;
    public Button hashAllFile;
    public Button reset;
    public Button moveToBuffer;

    public ImageView xorTip;
    public ImageView bufferTip;
    public ImageView resultTip;
    public ImageView readTip;
    public ImageView saveEncryptedTip;

    public ByteListController byteListController;
    public VBox byteList;

    public ToggleGroup group;
    public RadioButton fileOption;
    public RadioButton byteListOption;

    public Label controlTip;
    public Label ivTip;
    public Label keyTip;
    public Label byteListTip;
    public Label loadFileTip;
    public Label chooseSourceTip;


    private SimpleIntegerProperty state = new SimpleIntegerProperty();

    private DESHash des = new DESHash();
    private DESHash.ReadInterface readInterface = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        state.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            switch (newValue.intValue()){
                case START:
                    start();
                    break;
                case ADDITION:
                    addition();
                    break;
                case DES:
                    des();
                    break;
                case END:
                    end();
                    break;
            }
        });

        state.set(START);

        keyField.setTextFormatter(new TextFormatter<>(new HexTextFilter(14)));
        initializationField.setTextFormatter(new TextFormatter<>(new HexTextFilter(16)));

        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(fileOption.isSelected()){
                byteListController.setDisable(true);
                setDisableChooseFile(false);
            }
            else {
                setDisableChooseFile(true);

                try {
                    if(readInterface != null) readInterface.close();
                    readInterface = byteListController.getListSource();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                byteListController.setDisable(false);
            }
        });
    }

    private void start(){
        //set all buttons in initial position
        load.setDisable(false);
        makeStep.setDisable(false);
        hashAllFile.setDisable(false);
        moveToBuffer.setDisable(false);
        generateKey.setDisable(false);

        //set all tips in initial position
        setVisibleStartTips(true);
        setVisibleAdditionTips(false);
        setVisibleDesTips(false);
        setVisibleEndTips(false);

        //set all text fields in initial position
        encryptedField.setText("0000000000000000");
        textField.setText("00");
        bufferField.setText("");

        keyField.setText("");
        initializationField.setText("0000000000000000");

        keyField.setDisable(false);
        initializationField.setDisable(false);

        filePath.setText(null);
        length.setText(null);

        setDisableChooseSource(false);

        if(fileOption.isSelected()){
            byteListController.setDisable(true);
            setDisableChooseFile(false);
        }
        else{
            byteListController.setDisable(false);
            setDisableChooseFile(true);
        }
    }

    private synchronized void addition(){
        bufferField.setText(MyUtils.byteArrayToHexString(des.getBuffer()));

        setVisibleAdditionTips(true);
        setVisibleDesTips(false);

        if(!readInterface.hasData()) state.set(END);
    }

    private synchronized void des(){
        textField.setText(MyUtils.byteArrayToHexString(des.getText()));
        encryptedField.setText(MyUtils.byteArrayToHexString(des.getEncrypted()));

        setVisibleAdditionTips(false);
        setVisibleDesTips(true);

        updateFileLength(readInterface.getRemainedLength());
    }

    private synchronized void end(){
        reset.setDisable(false);
        makeStep.setDisable(true);
        hashAllFile.setDisable(true);

        setVisibleAdditionTips(false);
        setVisibleEndTips(true);

        textField.setText(MyUtils.byteArrayToHexString(des.getText()));
        encryptedField.setText(MyUtils.byteArrayToHexString(des.getEncrypted()));
        bufferField.setText(MyUtils.byteArrayToHexString(des.getBuffer()));

        updateFileLength(readInterface.getRemainedLength());
    }

    private boolean isValidForStep(){
        return readInterface != null && readInterface.hasData() &&
                keyField.getText() != null && keyField.getText().length() == 14 &&
                bufferField.getText() != null && bufferField.getText().length() == 16;
    }

    private void beforeMakeStepStarted(){
        keyField.setDisable(true);
        initializationField.setDisable(true);

        load.setDisable(true);
        generateKey.setDisable(true);
        moveToBuffer.setDisable(true);
        makeStep.setDisable(false);
        if(fileOption.isSelected()) hashAllFile.setDisable(false);

        setVisibleStartTips(false);

        setDisableChooseSource(true);
        byteListController.setDisable(true);
    }

    private void beforeHasAllFileStarted(){
        keyField.setDisable(true);
        initializationField.setDisable(true);
        load.setDisable(true);
        generateKey.setDisable(true);
        makeStep.setDisable(true);
        hashAllFile.setDisable(true);
        reset.setDisable(true);

        setVisibleStartTips(false);

        setDisableChooseSource(true);
        byteListController.setDisable(true);
    }

    private void setDisableChooseFile(boolean disable){
        load.setDisable(disable);
        filePath.setText(null);
        length.setText(null);

        hashAllFile.setDisable(disable);
    }

    private void setDisableChooseSource(boolean disable){
        fileOption.setDisable(disable);
        byteListOption.setDisable(disable);
    }

    private void setVisibleStartTips(boolean visible){
        controlTip.setVisible(visible);
        ivTip.setVisible(visible);
        keyTip.setVisible(visible);
        byteListTip.setVisible(visible);
        loadFileTip.setVisible(visible);
        chooseSourceTip.setVisible(visible);
    }

    private void setVisibleAdditionTips(boolean visible){
        bufferTip.setVisible(visible);
        xorTip.setVisible(visible);
    }

    private void setVisibleDesTips(boolean visible){
        readTip.setVisible(visible);
        saveEncryptedTip.setVisible(visible);
    }

    private void setVisibleEndTips(boolean visible){
        resultTip.setVisible(visible);
    }

    private void updateFileLength(long fileLength){
        length.setText(String.format(Locale.US, "Осталось байт: %d", fileLength));
    }

    private void showErrorDialog(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText("Для начала хэширования необходимо выбрать файл или заполнить таблицу байтов, ввести или сгенерировать ключ длинной в 14 символов и заполнить буффер из вектора инициализации длинной в 16 символов");
        alert.showAndWait();
    }

    private void showWrongDataDialog(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Внимание");
        alert.setHeaderText(null);
        alert.setContentText("Данные должны состоять из 16 символов.");
        alert.showAndWait();
    }


    public void onMakeStep(ActionEvent actionEvent) {
        switch (state.get()){
            case START:
                if(isValidForStep()){
                    beforeMakeStepStarted();

                    des.init(readInterface,
                            MyUtils.hexStringToByteArray(keyField.getText()),
                            MyUtils.hexStringToByteArray(textField.getText()),
                            MyUtils.hexStringToByteArray(bufferField.getText()),
                            MyUtils.hexStringToByteArray(encryptedField.getText()));
                    des.desStep();

                    state.set(DES);
                }
                else showErrorDialog();
                break;
            case ADDITION:
                des.desStep();
                state.set(DES);
                break;
            case DES:
                des.add();
                state.set(ADDITION);
                break;
        }
    }

    public void onHashAllFile(ActionEvent actionEvent) {
        if(isValidForStep()){
            beforeHasAllFileStarted();

            new Thread(() ->{
                des.init(readInterface,
                        MyUtils.hexStringToByteArray(keyField.getText()),
                        MyUtils.hexStringToByteArray(textField.getText()),
                        MyUtils.hexStringToByteArray(bufferField.getText()),
                        MyUtils.hexStringToByteArray(encryptedField.getText()));
                des.desStep();

                int counter = 0;
                int timesUpdated = 0;
                while (readInterface.hasData()){
                    if(counter == (timesUpdated > 15 ? 500000 : 15000)){
                        Platform.runLater(() -> state.set(DES));
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Platform.runLater(() -> state.set(ADDITION));
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        ++timesUpdated;
                        counter = 0;
                    }
                    else{
                        if(counter % 2 == 0) des.add();
                        else des.desStep();
                    }

                    ++counter;
                }

                Platform.runLater(() -> state.set(END));
            }).start();
        }
        else showErrorDialog();
    }

    public void onReset(ActionEvent actionEvent) {
        try {
            if(readInterface != null) readInterface.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        start();
        state.set(START);
    }

    public void onLoadFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(root.getScene().getWindow());
        if(file != null){
            try {
                readInterface = new FileSource(file);

                filePath.setText(file.getPath());
                updateFileLength(readInterface.getRemainedLength());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onGenerateKey(ActionEvent actionEvent) {
        try {
            keyField.setText(MyUtils.byteArrayToHexString(des.generateKey()));
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void moveToBuffer(ActionEvent actionEvent) {
        if(initializationField.getText() != null && initializationField.getText().length() == 16){
            moveToBuffer.setDisable(true);
            bufferField.setText(initializationField.getText());
        }
        else showWrongDataDialog();
    }
}


