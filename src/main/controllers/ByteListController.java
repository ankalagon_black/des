package main.controllers;

import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.model.ListSource;
import main.utils.HexTextFilter;
import main.views.ByteCell;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class ByteListController implements Initializable {
    public TextField newListItem;
    public Button addToList;
    public ListView<String> listView;
    public Button clear;
    public Label count;

    private ListSource listSource = new ListSource();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newListItem.setTextFormatter(new TextFormatter<>(new HexTextFilter(2)));
        listView.setCellFactory(param -> {
            try {
                return new ByteCell();
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        });
        listView.setItems(listSource.getList());

        listSource.getList().addListener((ListChangeListener<String>) c -> {
            boolean structural = false;
            while (c.next()){
                if (c.wasAdded() || c.wasRemoved()){
                    structural = true;
                    break;
                }
            }

            if(structural) updateCount();
        });
    }

    public void setDisable(boolean disable){
        newListItem.setDisable(disable);
        addToList.setDisable(disable);
        listView.setDisable(disable);
        clear.setDisable(disable);
    }

    private void showWrongFormatDialog(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Внимание");
        alert.setHeaderText(null);
        alert.setContentText("Данные должны состоять из 2-x символов.");
        alert.showAndWait();
    }

    private void updateCount(){
        count.setText(String.format(Locale.US, "Список байтов (%d)", listSource.getList().size()));
    }


    public ListSource getListSource(){
        return listSource;
    }


    public void onAddToList(ActionEvent actionEvent) {
        if(newListItem.getText() != null && newListItem.getText().length() == 2){
            listSource.getList().add(newListItem.getText());
        }
        else showWrongFormatDialog();
    }

    public void onClearList(ActionEvent actionEvent) {
        listSource.getList().clear();
    }
}
