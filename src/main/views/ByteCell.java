package main.views;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

import java.io.IOException;

public class ByteCell extends ListCell<String> {
    private Label label;

    public ByteCell() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/main/resources/fxml/byte_cell.fxml"));

        label = fxmlLoader.load();
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty && item != null){
            label.setText(item);
            setGraphic(label);
        }
        else{
            label.setText(null);
            setGraphic(null);
        }
    }
}
