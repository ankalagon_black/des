package main.model;

import main.utils.MyUtils;

public class KeyGenerator {

    private static int[] C0_permutation = {
            56,	48,	40,	32,	24,	16,	8,	0,	57,	49,	41,	33,	25,	17,
            9,	1,	58,	50,	42,	34,	26,	18,	10,	2,	59,	51,	43,	35
        };

    private static int[] D0_permutation = {
            62,	54,	46,	38,	30,	22,	14,	6,	61,	53,	45,	37,	29,	21,
            13,	5,	60,	52,	44,	36,	28,	20,	12,	4,	27,	19,	11,	3
    };

    private static int[] shiftTable = {1,	1,	2,	2,	2,	2,	2,	2,	1,	2,	2,	2,	2,	2,	2,	1};

    private static int[] keyPermutation = {13,	16,	10,	23,	0,	4,	2,	27,	14,	5,	20,	9,	22,	18,	11,	3, 25,	7,	15,	6,	26,	19,	12,	1,	40,	51,	30,	36,	46,	54,	29,	39, 50,	44,	32,	47,	43,	48,	38,	55,	33,	52,	45,	41,	49,	35,	28,	31};


    private Support support = new Support();

    private byte[][] keys = new byte[16][48];


    public KeyGenerator(byte[] initialKey){
        initialKey = MyUtils.toBits(initialKey, 7);

        byte[] extendedKey = extend(initialKey);

        byte[] c = support.permutation(extendedKey, C0_permutation),
                d = support.permutation(extendedKey, D0_permutation);

        for (int i = 0; i < 16; i++){
            c = shiftLeft(c, shiftTable[i]);
            d = shiftLeft(d, shiftTable[i]);

            keys[i] = support.permutation(support.concatenate(c,d), keyPermutation);
        }
    }

    public byte[] getKey(int i){
        return keys[i];
    }


    private byte[] extend(byte[] initialKey){
        byte[] extendedKey = new byte[64];
        int count = 0, times = 0;
        for (int i = 0; i < 64; i++){
            if(i % 8 == 7){
                extendedKey[i] = (byte) (count % 2 == 0 ? 1 : 0);
                ++times;
                count = 0;
            }
            else{
                byte value = initialKey[i - times];
                extendedKey[i] = value;
                count += value;
            }
        }
        return extendedKey;
    }

    private byte[] shiftLeft(byte[] origin, int positions){
        byte[] part1 = new byte[origin.length - positions],
                part2 = new byte[positions];

        System.arraycopy(origin, positions, part1, 0,origin.length - positions);
        System.arraycopy(origin, 0, part2, 0, positions);

        return support.concatenate(part1, part2);
    }
}
