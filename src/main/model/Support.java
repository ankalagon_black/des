package main.model;

public class Support {
    public byte[] concatenate(byte[] c, byte[] d){
        byte[] result = new byte[c.length + d.length];

        System.arraycopy(c, 0, result, 0, c.length);
        System.arraycopy(d, 0, result, c.length, d.length);

        return result;
    }

    public byte[] permutation(byte[] origin, int[] order){
        byte[] result = new byte[order.length];

        for (int i = 0; i < order.length; i++) result[i] = origin[order[i]];

        return result;
    }

    public void outArray(String title, byte[] bytes){
        System.out.println(title);
        for (byte aByte : bytes) {
            System.out.print(aByte);
        }
        System.out.println();
    }
}
