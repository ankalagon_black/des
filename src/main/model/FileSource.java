package main.model;

import main.utils.MyUtils;

import java.io.*;

public class FileSource implements DESHash.ReadInterface {
    private InputStream inputStream;
    private int lastIndex = 0;
    private long bytesRemained;

    public FileSource(File file) throws FileNotFoundException {
        inputStream = new FileInputStream(file);
        this.bytesRemained = file.length();
    }

    @Override
    public byte[] read() throws IOException {
        byte[] results = new byte[1];
        lastIndex = inputStream.read();
        if(lastIndex != -1) results[0] = (byte) lastIndex;
        
        bytesRemained = MyUtils.clampMin(0, bytesRemained - 1);

        return results;
    }

//    private int readEightBytes(byte[] results) throws IOException{
//        int count = 0;
//        int res = 0;
//        while (count < 8 && (res = inputStream.read()) != -1){
//            results[count] = (byte) res;
//            ++count;
//        }
//
//        for (int i = count; i < 8; i++){
//            results[i] = 0;
//        }
//
//        return res;
//    }

    @Override
    public boolean hasData() {
        return lastIndex != -1;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

    @Override
    public long getRemainedLength() {
        return bytesRemained;
    }
}
