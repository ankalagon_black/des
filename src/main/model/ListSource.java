package main.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.utils.MyUtils;

import java.io.IOException;

public class ListSource implements DESHash.ReadInterface {
    private ObservableList<String> list = FXCollections.observableArrayList();

    public ObservableList<String> getList() {
        return list;
    }

    @Override
    public byte[] read() throws IOException {
        byte[] results = new byte[1];

        if(list.size() != 0) results[0] = MyUtils.hexStringToByte(list.remove(0));
        else results[0] = 0;

        return results;
    }

//    private byte[] readEightBytes() throws IOException{
//        byte[] results = new byte[8];
//        int count = 0;
//        while (count < 8 && list.size() != 0){
//            results[count] = MyUtils.hexStringToByte(list.remove(list.size() - 1));
//            ++count;
//        }
//
//        for (int i = count; i < 8; i++){
//            results[i] = 0;
//        }
//
//        return results;
//    }

    @Override
    public boolean hasData() {
        return list.size() != 0;
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public long getRemainedLength() {
        return list.size();
    }
}
