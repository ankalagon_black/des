package main.model;

import javax.crypto.KeyGenerator;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class DESHash {
    private byte[] text, buffer, encrypted;

    private ReadInterface readInterface;

    private DES des = new DES();

    public void init(ReadInterface readInterface, byte[] key, byte[] text, byte[] buffer, byte[] encrypted){
        this.readInterface = readInterface;

        this.text = text;
        this.buffer = buffer;
        this.encrypted = encrypted;

        des.init(key);
    }

    public void desStep(){
        try {
            encrypted = des.encode(buffer);
            text = readInterface.read();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void add(){
        System.arraycopy(buffer, 1, buffer, 0, 7);
        buffer[7] = xor(text, encrypted);
    }

    public byte[] generateKey() throws NoSuchAlgorithmException {
        byte[] encoded = KeyGenerator.getInstance("DES").generateKey().getEncoded();
        byte[] result = new byte[7];

        System.arraycopy(encoded, 0, result, 0, 7);

        return result;
    }


    public byte[] getText() {
        return text;
    }

    public byte[] getEncrypted() {
        return encrypted;
    }

    public byte[] getBuffer() {
        return buffer;
    }


    private byte xor(byte[] text, byte[] encrypted){
        return (byte) (((int) text[0]) ^ ((int) encrypted[7]));
    }

    public interface ReadInterface{
        byte[] read() throws IOException;
        boolean hasData();
        void close() throws IOException;

        long getRemainedLength();
    }
}
