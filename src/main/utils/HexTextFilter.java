package main.utils;

import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;

public class HexTextFilter implements UnaryOperator<TextFormatter.Change> {
    private String length;

    public HexTextFilter(int length){
        this.length = String.valueOf(length);
    }

    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {
//        int from = change.getRangeStart();
//
//        if(from >= length){
//            change.setText("");
//            return change;
//        }
//        else {
//            String allowedCharacters = change.getText().replaceAll("[^0-9A-Fa-f]*", "");
//            int to = from + allowedCharacters.length();
//
//            if (to >= length) {
//                int allowedLength = length - from;
//                change.setText(allowedCharacters.substring(0, allowedLength));
//            }
//            else change.setText(allowedCharacters);



        return change.getControlNewText().matches("[0-9A-Fa-f]{0," + length + "}") ? change : null;
    }
}
