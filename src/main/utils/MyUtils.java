package main.utils;

public class MyUtils {

    public static String byteArrayToHexString(byte[] array){
        StringBuilder hex = new StringBuilder();
        for (byte i: array){
            String hexStr = Integer.toHexString(i & 0xFF).toUpperCase();

            int diff = 2 - hexStr.length();
            if(diff != 0) hex.append(String.format("%0" + (diff) + "d%s", 0, hexStr));
            else hex.append(hexStr);
        }

        return hex.toString();
    }

    public static byte[] hexStringToByteArray(String hexString){
        byte[] bytes = new byte[8];

        int length = hexString.length();
        for (int i = 0; i < length; i += 2){
            bytes[i / 2] = (byte) Integer.parseInt(hexString.substring(i, Math.min(i + 2, length)), 16);
        }

        return bytes;
    }

    public static byte hexStringToByte(String hexString){
        return (byte) Integer.parseInt(hexString, 16);
    }

    public static long clampMin(long min, long value){
        if (value < min) return min;
        else return value;
    }

    public static byte toByte(byte[] array){
        byte mask = 1;
        byte result = 0;

        for (int i = array.length - 1; i >= 0; i--){
            result |= (mask & array[i]) << (array.length - 1 - i);
        }

        return result;
    }

    public static byte[] toBits(byte b, int size){
        byte mask = 1;
        byte[] result = new byte[size];

        for (int i = size - 1; i >= 0; i--){
            result[i] = (byte)((int) b & (int) mask) == mask ? (byte) 1 : (byte) 0;
            mask <<= 1;
        }

        return result;
    }

    public static byte[] toBits(byte[] bytes, int size){
        byte[] result = new byte[size * 8];

        int minSize = Math.min(size, bytes.length);
        int diff = size - bytes.length;

        for (int i = 0; i < minSize; i++){
            System.arraycopy(MyUtils.toBits(bytes[i], 8), 0, result, i * 8, 8);
        }

        byte[] padding = new byte[] {0, 0, 0, 0, 0, 0, 0, 0};

        for(int i = 0; i < diff; i++){
            System.arraycopy(padding, 0, result, (i + minSize) * 8, 8);
        }

        return result;
    }

    public static byte[] toBytes(byte[] bits){
        int size = (int) Math.ceil(bits.length / 8);

        byte[] bytes = new byte[size];

        for (int i = 0; i < size; i++){
            byte[] buffer = new byte[8];
            for (int j = 0; j < 8; j++){
                int index = i * 8 + j;
                if(index < bits.length) buffer[j] = bits[index];
                else buffer[j] = 0;
            }
            bytes[i] = MyUtils.toByte(buffer);
        }

        return bytes;
    }
}

