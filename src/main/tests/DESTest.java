package main.tests;

import main.model.DES;
import main.model.KeyGenerator;
import main.model.Support;
import main.utils.MyUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class DESTest {

    @Test
    void add() {
//        byte[] word = {
//                0,0,0,0,0,0,0,0,
//                0,0,0,0,0,0,0,1,
//                0,0,0,0,0,0,1,0,
//                0,0,0,0,0,0,1,1,
//                0,0,0,0,0,1,0,0,
//                0,0,0,0,0,1,0,1,
//                0,0,0,0,0,1,1,0,
//                0,0,0,0,0,1,1,1};
//
//        byte[] key = {
//                0,0,0,0,0,0,0,0,
//                0,0,0,0,0,0,0,1,
//                0,0,0,0,0,0,1,0,
//                0,0,0,0,0,0,1,1,
//                0,0,0,0,0,1,0,0,
//                0,0,0,0,0,1,0,1,
//                0,0,0,0,0,1,1,0};
//
        Support support = new Support();

        DES des = new DES();
        byte[] word = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7};
        byte[] key = new byte[]{ 0, 1, 2, 3, 4, 5, 6};
        des.init(key);

        support.outArray("title", MyUtils.toBits(des.encode(word), 8));
    }
}